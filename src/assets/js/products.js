const products = [
    { 
        image: 'https://jezblackmore.com/inov8home/shoeSelect.png',
        title: 'F-Lite G 300 Graphite Pack',
        price: 140,
        onSale: false,
        salePrice: 0,
        linkMen: '#',
        linkWomen: '#',
        feature: "Limited Edition"
    },
    { 
        image: 'https://jezblackmore.com/inov8home/shoeSelect.png',
        title: 'Teraultra G 270',
        price: 140,
        onSale: true,
        salePrice: 100,
        linkMen: '#',
        linkWomen: '#',
        feature: ""
    },
    { 
        image: 'https://jezblackmore.com/inov8home/shoeSelect.png',
        title: 'Roclite Pro G 400 FTX',
        price: 200,
        onSale: false,
        salePrice: 0,
        linkMen: '#',
        linkWomen: '#',
        feature: "Award Winning Hiking Boot"
    },
    { 
        image: 'https://jezblackmore.com/inov8home/shoeSelect.png',
        title: 'F-Lite G 300 Graphite Pack',
        price: 140,
        onSale: false,
        salePrice: 0,
        linkMen: '#',
        linkWomen: '#',
        feature: ""
    },
    { 
        image: 'https://jezblackmore.com/inov8home/shoeSelect.png',
        title: 'Teraultra G 270',
        price: 140,
        onSale: true,
        salePrice: 100,
        linkMen: '#',
        linkWomen: '#',
        feature: ""
    },
    { 
        image: 'https://jezblackmore.com/inov8home/shoeSelect.png',
        title: 'Roclite Pro G 400 FTX',
        price: 200,
        onSale: false,
        salePrice: 0,
        linkMen: '#',
        linkWomen: '#',
        feature: ""
    },
]

export default products;